import { admins } from "./mock";
import { LoginData } from "../types";

export const checkAdmin = (logData: LoginData) => {
  return admins.some(
    ({ email, password }) =>
      email === logData.email && password === logData.password,
  );
};
