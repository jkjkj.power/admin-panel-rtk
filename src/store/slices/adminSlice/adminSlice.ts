import { createSlice } from "@reduxjs/toolkit";
import type { PayloadAction } from "@reduxjs/toolkit";
import { checkAdmin } from "./utils";
import { LoginData } from "../types";

const initialState = {
  email: "",
  password: "",
};

export const adminSlice = createSlice({
  name: "admin",
  initialState,
  reducers: {
    login: (state, action: PayloadAction<LoginData>) => {
      if (checkAdmin(action.payload)) {
        state.email = action.payload.email;
        state.password = action.payload.password;
      }
    },
  },
});

export const { login } = adminSlice.actions;

export const adminReducer = adminSlice.reducer;
