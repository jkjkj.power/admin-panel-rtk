import { NotificationEnum, StatusEnum } from "./constants";

export type LoginData = {
  email: string;
  password: string;
};

export type User = {
  id: string;
  name: string;
  surName: string;
  status: StatusEnum | undefined;
  isModerator: boolean;
};

export type NotificationState = {
  type: NotificationEnum;
  recipients: User[];
  theme: string;
  description: string;
};

export type Suggestion = {
  id: string;
  user: User;
  theme: string;
  text: string;
  date: string;
  status: StatusEnum;
};

export type SuggestionState = {
  suggestions: Suggestion[];
  selectedSuggestion: Suggestion | undefined;
};

export type LearningState = {
  users: User[];
  actionsGoals: ActionGoalsType[];
  selectedActionGoal: ActionGoalsType;
};

export type Goal = {
  field: string;
  gifts: Gift[];
  steps: Step[];
};

type Action = {
  result: string;
  time: string;
};

type ActionGoal<T> = T & {
  id: string;
  userId: string;
  name: string;
  date: string;
  points: number;
};

export type Gift = string;
export type Step = string;

export type ActionGoalsType = ActionGoal<Action | Goal>;
