import { createSlice } from "@reduxjs/toolkit";
import type { PayloadAction } from "@reduxjs/toolkit";
import { NotificationEnum } from "../constants";
import { NotificationState, User } from "../types";

const initialState: NotificationState = {
  type: NotificationEnum.SYSTEM,
  recipients: [],
  theme: "",
  description: "",
};

export const notificationSlice = createSlice({
  name: "notification",
  initialState,
  reducers: {
    setNotificationType: (state, action: PayloadAction<NotificationEnum>) => {
      state.type = action.payload;
    },
    setRecipient: (state, action: PayloadAction<User>) => {
      state.recipients = [...state.recipients, action.payload];
    },
    setTheme: (state, action: PayloadAction<string>) => {
      state.theme = action.payload;
    },
    setDescription: (state, action: PayloadAction<string>) => {
      state.description = action.payload;
    },
  },
});

export const { setNotificationType, setRecipient, setTheme, setDescription } =
  notificationSlice.actions;

export const notificationReducer = notificationSlice.reducer;
