import type { PayloadAction } from "@reduxjs/toolkit";
import { createSlice } from "@reduxjs/toolkit";
import {
  ActionGoalsType,
  Gift,
  Goal,
  LearningState,
  Step,
  User,
} from "../types";

const initialState: LearningState = {
  users: [] as User[],
  actionsGoals: [] as ActionGoalsType[],
  selectedActionGoal: undefined,
};

export const learningSlice = createSlice({
  name: "lerning",
  initialState,
  reducers: {
    setUsers: (state, action: PayloadAction<User[]>) => {
      state.users = action.payload;
    },
    setActionsGoals: (state, action: PayloadAction<ActionGoalsType[]>) => {
      state.actionsGoals = action.payload;
    },
    setCurrentActionGoal: (state, action: PayloadAction<ActionGoalsType>) => {
      state.selectedActionGoal = action.payload;
    },
    updateActionsGoals: (state, action: PayloadAction<ActionGoalsType>) => {
      state.actionsGoals = [...state.actionsGoals, action.payload];
    },

    // TODO: вынести логику в хэндлер, через который будет дергаться этот метод
    addGoalGift: (
      state,
      action: PayloadAction<{ actionGoalId: string; gift: Gift }>,
    ) => {
      const updatedGoals = state.actionsGoals.map((item) => {
        if (item.id === action.payload.actionGoalId) {
          (item as Goal).gifts.push(action.payload.gift);

          return item;
        }
      });

      state.actionsGoals = updatedGoals;
    },

    // TODO: вынести логику в хэндлер, через который будет дергаться этот метод
    addGoalStep: (
      state,
      action: PayloadAction<{ actionGoalId: string; step: Step }>,
    ) => {
      const updatedGoals = state.actionsGoals.map((item) => {
        if (item.id === action.payload.actionGoalId) {
          (item as Goal).gifts.push(action.payload.step);

          return item;
        }
      });

      state.actionsGoals = updatedGoals;
    },
  },
});

export const {
  setUsers,
  setActionsGoals,
  setCurrentActionGoal,
  updateActionsGoals,
  addGoalGift,
  addGoalStep,
} = learningSlice.actions;

export const learningReducer = learningSlice.reducer;
