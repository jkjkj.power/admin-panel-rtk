import { createSlice } from "@reduxjs/toolkit";
import type { PayloadAction } from "@reduxjs/toolkit";
import { User } from "../types";

const initialState: User = {
  id: "",
  name: "",
  surName: "",
  isModerator: false,
  status: undefined,
};

export const userSlice = createSlice({
  name: "user",
  initialState,
  reducers: {},
});

export const {} = userSlice.actions;

export const userReducer = userSlice.reducer;
