export enum NotificationEnum {
  SYSTEM = "system",
}

export enum StatusEnum {
  // для модераторов/пользователей
  ACTIVE = "active",
  SUSPENDED = "suspended",
  DELETED = "deleted",

  // для жалоб/предложений
  DECLINE = "decline",
  ACCEPTED = "accepted",
  REVIEW = "review",
}
