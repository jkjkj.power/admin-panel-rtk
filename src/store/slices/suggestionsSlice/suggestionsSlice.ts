import { createSlice } from "@reduxjs/toolkit";
import { Suggestion, SuggestionState } from "../types";
import type { PayloadAction } from "@reduxjs/toolkit";

const initialState: SuggestionState = {
  suggestions: [] as Suggestion[],
  selectedSuggestion: undefined,
};

export const suggestionsSlice = createSlice({
  name: "suggestions",
  initialState,
  reducers: {
    setSelectedSuggestion: (state, action: PayloadAction<Suggestion>) => {
      state.selectedSuggestion = action.payload;
    },
    updateSuggestions: (state, action: PayloadAction<Suggestion[]>) => {
      state.suggestions = action.payload;
    },
  },
});

export const { updateSuggestions, setSelectedSuggestion } =
  suggestionsSlice.actions;

export const suggestionsReducer = suggestionsSlice.reducer;
