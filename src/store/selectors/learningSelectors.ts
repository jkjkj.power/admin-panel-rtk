import { RootState } from "../store";

const learningSelector = (state: RootState) => state.learning;

export const actionsGoalsSelector = (state: RootState) =>
  learningSelector(state).actionsGoals;

export const usersSelector = (state: RootState) =>
  learningSelector(state).users;

export const selectedActionGoalSelector = (state: RootState) =>
  learningSelector(state).selectedActionGoal;
