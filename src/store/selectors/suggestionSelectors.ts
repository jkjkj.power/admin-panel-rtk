import { RootState } from "../store";

const suggestionSelector = (state: RootState) => state.suggestions;

export const suggestionsSelector = (state: RootState) =>
  suggestionSelector(state).suggestions;

export const selectedSuggestionSelector = (state: RootState) =>
  suggestionSelector(state).selectedSuggestion;
