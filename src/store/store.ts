import { configureStore } from "@reduxjs/toolkit";
import { notificationReducer } from "./slices/notificationSlice";
import { suggestionsReducer } from "./slices/suggestionsSlice";
import { adminReducer } from "./slices/adminSlice";
import { learningReducer } from "./slices/learningSlice";

export const store = configureStore({
  reducer: {
    admin: adminReducer,
    learning: learningReducer,
    suggestions: suggestionsReducer,
    notification: notificationReducer,
  },
});

export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;
